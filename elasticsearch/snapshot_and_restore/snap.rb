#!/usr/bin/env ruby

# Author: Jacob Royal
# Created: 2014-07-16
# Last Update: 2014-07-22
# This ruby script allows for creation of Elasticsearch snapshot images, creation of snapshot repositories
# and allows snapshots to be restored by passing in a series of options by using RESTful API calls

require 'rubygems'
require 'net/http'
require 'optparse'
require 'fileutils'

  # Take a snapshot backup of all indices
  def backup

    # set path used in the HTTP call
    path = "/_snapshot/#{OPTIONS[:repository]}/#{OPTIONS[:time]}?wait_for_completion=true"

    req = Net::HTTP::Put.new(path)
    response = Net::HTTP.new(OPTIONS[:host],OPTIONS[:port]).start { |http| http.request(req) }

    message(response)
  end

  # Create a snapshot repository
  #TODO: Figure out why passing a payload to Net::HTTP results in 500 error, RepositoryException.
  # See previous versions of this script for illustration.
  def create

    repo_locate = "#{OPTIONS[:location]}" + "#{OPTIONS[:repository]}"
    FileUtils.makedirs(repo_locate) unless File.exists?(repo_locate)
    FileUtils.chown_R('elasticsearch', 'elasticsearch', OPTIONS[:location])

    path = "/_snapshot/#{OPTIONS[:repository]}"
    url = "http://#{OPTIONS[:host]}:#{OPTIONS[:port]}#{path}"

    `/usr/bin/curl -XPUT #{url} -d '{
      "type": "fs",
      "settings": {
        "location": "#{repo_locate}",
        "compress": true
      }
    }'`

  end

  # Restore all indices from a snapshot file
  def restore
    snap_locate = "#{OPTIONS[:location]}" + "#{OPTIONS[:repository]}/" + "snapshot-#{OPTIONS[:name]}"

    if File.exist?(snap_locate)

      # Close all indices
      close_all = "/_all/_close"
      req = Net::HTTP::Post.new(close_all)
      close = Net::HTTP.new(OPTIONS[:host], OPTIONS[:port]).start {|http| http.request(req)}

      message(close)

      path = "/_snapshot/#{OPTIONS[:repository]}/#{OPTIONS[:name]}/_restore"

      restore = Net::HTTP::Post.new(path)
      response = Net::HTTP.new(OPTIONS[:host], OPTIONS[:port]).start {|http| http.request(restore)}

      message(response)
    else
      puts "[ERROR] the snapshot you specified (#{snap_locate}) does not exist\n" +
            "\tUse the -L option to view a list of all repositories\n"+
            "\tUse the -r <repoName> -V options to view a list of available snapshots in a repository"
      exit
    end
  end

  # List all created repositories
  def snap_list
    path = "/_snapshot/#{OPTIONS[:repository]}/_all?pretty"

    req = Net::HTTP::Get.new(path)
    response = Net::HTTP.new(OPTIONS[:host], OPTIONS[:port]).start {|http| http.request(req)}

    message(response)
  end

  def repo_list
    path = '/_snapshot/_all?pretty'

    req = Net::HTTP::Get.new(path)
    response = Net::HTTP.new(OPTIONS[:host], OPTIONS[:port]).start {|http| http.request(req)}

    message(response)
  end

  # List all available snapshots for a given repository
  def snap_info
    path = "/_snapshot/#{OPTIONS[:repository]}/#{OPTIONS[:name]}?pretty"

    req = Net::HTTP::Get.new(path)
    response = Net::HTTP.new(OPTIONS[:host], OPTIONS[:port]).start {|http| http.request(req)}

    message(response)
  end

  def delete
    path = "/_snapshot/#{OPTIONS[:repository]}/#{OPTIONS[:name]}"

    req = Net::HTTP::Delete.new(path)
    response = Net::HTTP.new(OPTIONS[:host], OPTIONS[:port]).start {|http| http.request(req)}

    message(response)
  end

  def message(response)
    if response.code =='200'
      puts response.body
    else
      puts "[ERROR] #{Time.now} #{response.code} #{response.body}"
    end
end

#################   MAIN   ###################

# declare global variables

OPTIONS = {} # holds all the cli options and default values
  # Setting default values
  OPTIONS[:host]        = 'localhost'
  OPTIONS[:port]        = '9200'
  OPTIONS[:time]        = Time.now.strftime('%F:%T')
  OPTIONS[:location]    = '/var/lib/es_backup/'
  OPTIONS[:repository]  = 'es_snapshots'

#### Parse cli options ####
optparse = OptionParser.new do|opts|
  opts.banner = 'Usage: ruby snap.rb [options]'

  opts.on( '-r', '--repository REPO', 'Name of the snapshot repository',
            'default: es_snapshots') do |x|
    OPTIONS[:repository]= x
  end

  opts.on( '-a', '--action TYPE', [:backup, :restore, :create],
           'Select action (shoot, restore, create)',
            "\tbackup: create snapshot backup of all indices",
            "\trestore: close all indices and restore",
            "\t  them to given snapshot",
            "\tcreate: create a snapshot repository",
            "\t NOTE: must be unique name") do |x|
    OPTIONS[:action] = x
  end

  opts.on( '-l', '--location PATH', 'Where the repository resides',
           '  default: /var/lib/es-snapshots/') do |x|
    OPTIONS[:location] = x
  end

  opts.on( '-n', '--name [NAME]', 'Snapshot name (used in restore and delete only)') do |x|
    OPTIONS[:name] = x
  end

  opts.on( '-V', 'View all snapshot info in a repository') do
    snap_list
    exit
  end

  opts.on( '-L', 'List all repository info') do
    repo_list
    exit
  end

  opts.on( '-S', 'Use with -r and -n options to list individual',
                  'snapshot information') do
    snap_info
    exit
  end

  opts.on( '--delete', 'Delete repository or snapshot')do
    puts "Caution: deleting repositories/snapshots can result in data loss\nDo you wish to continue? (y/n) "
    cont = gets.chomp
    case cont.downcase
      when 'y'
        delete
        exit
      else
        exit
    end
  end

  opts.on( '-h', '--help', 'Display this message' ) do
    puts opts
    puts "\nExample Usage: \nruby snap.rb -r repo1 -V\n\t Lists all snapshot info for repo1\n" +
          "ruby snap.rb -L \n\t List all repositories and their info\n" +
          "ruby snap.rb -r repo1 -n snapshot1 -a restore\n" +
          "\tRestore elasticsearch to snapshot1 from repo1\n" +
          "ruby snap.rb -r repo1 -a backup\n" +
          "\tTake a snapshot backup and put the snapshot in repo1\n" +
          "ruby snap.rb -r repo1 --delete\n" +
          "\tWill delete repo1 and all its contents\n" +
          "ruby snap.rb -a create\n" +
          "\tWill create a repository with the default name of es_snapshots"
    exit
  end
end

optparse.parse!

#### Action decision ####
case OPTIONS[:action].to_s
  when 'backup'
    backup
  when 'create'
    create
  when 'restore'
    restore
  else
    puts 'Oops! Something went wrong!'
end